import java.util.Arrays;
import java.util.LinkedList;

public class SortingAlgorithms {
	private VisualSorting vs;
	private int list[];
	public void setList(int[] list) {
		this.list = list;
	}

	public SortingAlgorithms(VisualSorting vs) {
		this.vs = vs;

	};
	public void swapAnimate(int i,int j)
	{
		vs.swap = i;
		int swapX = i*vs.width*2 + (int)vs.width/20;
		MainInterface.SwapX = swapX;
		vs.swap1 = j;
		int swap1X= j*vs.width*2 + (int)vs.width/20;
		MainInterface.SwapX1 = swap1X;
		vs.Update();
		vs.delay();
		//Swap animations
		while(MainInterface.SwapX>swap1X)
		{
			vs.Update(6);
			vs.delay(1);
			MainInterface.SwapX--;
			MainInterface.SwapX1++;
		}
		
	}
	public void selectionSort() {
		int len = VisualSorting.len;
		int list[] = VisualSorting.list;
		int check;
		int c = 0;
		while (c < len && vs.sorting) {
			int sm = c;
			vs.current = c;
			for (int i = c + 1; i < len; i++) {
				if (!vs.sorting)
					break;
				if (list[i] < list[sm]) {
					sm = i;
				}
				check = i;
				vs.check = check;
				vs.Update();
				vs.delay();
				vs.check = -1;
				vs.Update();
			}
			if (c != sm) {
				swapAnimate(sm, c);
				swap(c, sm);
				vs.swap=sm;
				vs.swap1=c;
				vs.Update(6, vs.width);
				vs.delay();
			}
			vs.swap = -1;
			vs.swap1 = -1;
			vs.Update();
			vs.delay();
			c++;
		}
		vs.reset();

	}

	public void insertionSort(int start, int end) {
		int width = vs.width;
		int len = vs.len;
		int list[] = VisualSorting.list;
		int check;
		for (int i = start + 1; i <= end; i++) {
			vs.current = i;
			vs.Update();
			vs.delay();
			int j = i;
			while (list[j] < list[j - 1] && vs.sorting) {
				vs.swap= j;
				vs.swap1 =j-1;
				int swapX1 = (int)(j-1) *width*3/2+(1000-len*width)/2;
				MainInterface.SwapX = (int)j *width*3/2+(1000-len*width)/2;
				MainInterface.SwapX1 = swapX1;
				vs.Update();
				vs.delay();
				
				while(MainInterface.SwapX>swapX1)
				{
					MainInterface.SwapX--;
					MainInterface.SwapX1++;
					vs.update(200-width,width);
					vs.delay(vs.spd/10);
				}
				swap(j, j - 1);
				if (j != i) {
					check = j;
					vs.check = check;
					vs.Update();
					vs.delay();
				}
				if (j > start + 1)
					j--;
			}
			vs.check = -1;
			vs.swap=-1;
			vs.swap1=-1;
			vs.Update();
		}

	}

	public void bubbleSort() {
		int len = VisualSorting.len;
		int current;
		int list[] = VisualSorting.list;
		int check;
		int c = 1;
		boolean noswaps = false;
		while (!noswaps && vs.sorting) {
			current = len - c;
			vs.current = current;
			noswaps = true;
			for (int i = 0; i < len - c; i++) {
				if (!vs.sorting)
					break;
				if (list[i + 1] < list[i]) {
					swapAnimate(i+1, i);
					noswaps = false;
					swap(i+1,i);
					vs.swap = i;
					vs.swap1 = i + 1;
					vs.Update(6, vs.width);
					vs.delay();
					vs.swap = -1;
					vs.swap1 = -1;

				}
				check = i + 1;
				vs.check = check;
				vs.Update();
				vs.delay();

			}
			vs.check = -1;
			vs.Update();
			c++;
		}
	}

	void merge(int l, int m, int r) {

		int list[] = VisualSorting.list;
		int n1 = m - l + 1;
		int n2 = r - m;

		int L[] = new int[n1];
		int R[] = new int[n2];
		for (int i = 0; i < list.length; i++) {
			MainInterface.temp.addLast(new LinkedList<Integer>());
		}

		for (int i = 0; i < n1; i++) {
			if (!vs.sorting)
				break;
			L[i] = list[l + i];
			MainInterface.checklist.add(l + i);

		}
		for (int j = 0; j < n2; j++) {
			if (!vs.sorting)
				break;
			R[j] = list[m + 1 + j];
			MainInterface.checklist.add(m + 1 + j);

		}
		vs.Update();
		vs.delay();
		int i = 0, j = 0;
		int k = l;
		while (i < n1 && j < n2 && vs.sorting) {
			if (L[i] <= R[j]) {
				MainInterface.temp.get(k).add(l + i);
				MainInterface.temp.get(k).add(L[i]);

				vs.Update(6);
				vs.delay();
				list[k] = L[i];
				i++;
			} else {
				MainInterface.temp.get(k).add(m + j + 1);
				MainInterface.temp.get(k).add(R[j]);
				vs.Update(6);
				vs.delay();
				list[k] = R[j];
				j++;
			}
			k++;
		}

		while (i < n1 && vs.sorting) {
			MainInterface.temp.get(k).add(l + i);
			MainInterface.temp.get(k).add(L[i]);
			vs.Update(6);
			vs.delay();
			list[k] = L[i];
			i++;
			k++;

		}

		while (j < n2 && vs.sorting) {
			MainInterface.temp.get(k).add(m + 1 + j);
			MainInterface.temp.get(k).add(R[j]);
			vs.Update(6);
			vs.delay();
			list[k] = R[j];
			j++;
			k++;

		}
		if (!vs.sorting) {
			vs.d.clear();
			vs.reset();
			vs.Update();
		}
		vs.d.remove(vs.d.size() - 1);
		vs.Update(6);
		vs.delay();
		MainInterface.checklist.clear();
		MainInterface.temp.clear();
		vs.Update();
	}

	public void mergeSort(int l, int r) {

		if (l < r && vs.sorting == true) {
			int m = (l + r) / 2;
			vs.d.add(m + 1);// add m+1 to divide list
			vs.Update();
			vs.delay();
			mergeSort(l, m);
			mergeSort(m + 1, r);
			merge(l, m, r);
		}
		vs.Update();
		vs.delay();
	}

	public int getMax(int n) {
		int []list = vs.list;
		int mx = list[0];
		for (int i = 1; i < n; i++) {
			if (list[i] > mx)
				mx = list[i];

		}
		return mx;
	}
	public void bucketSort() {
		int counter = 0;
		int width = vs.width;
		int len = vs.len;
		int list[] = VisualSorting.list;
		for (int i = 0; i < 10; i++) {
			MainInterface.allBuckets.addLast(new LinkedList<Integer>());
		}
		for (int i = 0; i < list.length; i++) {
			vs.check = i;
			vs.Update();
			vs.delay();
			MainInterface.startB = 1;
			vs.delay();
			int j = list[i] / 10;
			
			MainInterface.choiceB = i;
			MainInterface.moveBx = (int)i *width*3/2+(1000-len*width)/2;
			MainInterface.moveBy = 200+width;
			int listI = MainInterface.allBuckets.get(j).size();
			int tempX = (int)j *width*3/2+(1200-10*width)/2;
			int tempY= vs.SIZE-width-listI*width+(int)width/20;
			vs.Update();
			vs.delay();
			while(MainInterface.moveBx<tempX)
			{
				MainInterface.moveBx++;
				vs.update(200+width,width);
				vs.delay(2);
				
			}
			while(MainInterface.moveBx>tempX)
			{
				MainInterface.moveBx--;
				vs.update(200+width,width);
				vs.delay(2);
				
			}
			while(MainInterface.moveBy<tempY)
			{
				MainInterface.moveBy++;
				vs.update(200+width,600);
				vs.delay(2);
			}
			MainInterface.allBuckets.get(j).addLast(list[i]);
			vs.Update();
			vs.delay();
			MainInterface.choiceB =-1;
		}
		vs.check = -1;
		vs.Update();
		Arrays.fill(list, -1);
		vs.Update();
		vs.delay();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < MainInterface.allBuckets.get(i).size(); j++) {
				MainInterface.checkB1=i;
				MainInterface.checkB2=j;
				vs.Update();
				vs.delay();
				list[counter] = MainInterface.allBuckets.get(i).get(j);
				vs.Update();
				vs.delay(1);
				counter++;
			}
		}
		insertionSort(0, list.length - 1);
		MainInterface.startB=0;
		MainInterface.allBuckets.clear();
	}

	public void swap(int i1, int i2) {
		int list[] = vs.list;

		int temp = list[i1];

		list[i1] = list[i2];

		list[i2] = temp;

	}
}
