import java.awt.Graphics;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
public class WaterPipe extends JPanel{
	private BufferedImage bf;
	private int x;
	private int y;	
	private int width;
	private int height;
	private JPanel jf;
	public WaterPipe(int x,int y,int width,int height,JPanel jf)
	{
		this.x= x;
		this.y =y;
		this.width = width;
		this.height= height;
		this.jf=jf;
	}
	public void paint(Graphics g)
	{
		try {
			bf = ImageIO.read(new File("OngNuocDuoi.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		g.drawImage(bf,x,y,width,height,jf);
	}
}
