import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Tools extends JPanel {

	private String[] types = { "Mario", "Songoku" };
	private String[] algorithms = { "Selection Sort", "Bubble Sort", "Merge Sort", "BucketSort" };
	private String[] algInfo = { "Best Case: O(n^2)\nWorst Case: O(n^2)\nAverage: O(n^2)",
			"Best Case: O(n)\nWorst Case: O(n^2)\nAverage: O(n^2)",
			"Best Case: --\nWorst Case: O(n*(k/d))\nAverage: O(n*(k/d))",
			"Best Case: O(n)\nWorst Case: O(n*n!)\nAverage: O(oo)\n\nSee you after the heat death \nof the Uniiverse." };
	
	public Tools() {
		int spd = VisualSorting.spd;
		int len = VisualSorting.len;
		JPanel tools = this;
		int curAlg=0;
		JLabel delayL = new JLabel("Delay :");
		JLabel msL = new JLabel(spd +"");
		JLabel sizeL = new JLabel("Size :");
		JLabel lenL = new JLabel(len + "");
		JLabel algorithmL = new JLabel("Algorithms");
		JLabel typeL = new JLabel("Character");
		// DROP DOWN BOX
		JComboBox alg = new JComboBox(algorithms);
		JComboBox graph = new JComboBox(types);
		JTextArea information = new JTextArea(algInfo[curAlg]);
		// BUTTONS
		JButton sort = new JButton("Sort");
		JButton shuffle = new JButton("Shuffle");
		JButton credit = new JButton("Credit");
		// SLIDERS
		JSlider size = new JSlider(JSlider.HORIZONTAL, 1, 6, 1);
		JSlider speed = new JSlider(JSlider.HORIZONTAL, 0, 100, spd);
		
		
		// SET UP TOOLBAR
		tools.setLayout(null);
		
		

		// SET UP ALGORITHM LABEL
		algorithmL.setHorizontalAlignment(JLabel.CENTER);
		
		algorithmL.setBounds(40, 20, 100, 25);
		
		tools.add(algorithmL);

		// SET UP DROP DOWN
		alg.setBounds(30, 45, 120, 25);// top=30,left=45
		tools.add(alg);

		// SET UP GRAPH TYPE LABEL
		typeL.setHorizontalAlignment(JLabel.CENTER);
		typeL.setBounds(40, 80, 100, 25);
		tools.add(typeL);

		// SET UP GRAPH TYPE DROP DOWN
		graph.setBounds(30, 105, 120, 25);
		tools.add(graph);

		// SET UP SORT BUTTON
		sort.setBounds(40, 150, 100, 25);
		tools.add(sort);

		// SET UP SHUFFLE BUTTON
		shuffle.setBounds(40, 190, 100, 25);
		tools.add(shuffle);

		// SET UP DELAY LABEL
		delayL.setHorizontalAlignment(JLabel.LEFT);
		delayL.setBounds(10, 230, 50, 25);
		tools.add(delayL);

		// SET UP MS LABEL
		msL.setHorizontalAlignment(JLabel.LEFT);
		msL.setBounds(135, 230, 25, 25);
		tools.add(msL);

		// SET UP SPEED SLIDER
		speed.setMajorTickSpacing(5);
		speed.setBounds(55, 230, 75, 25);
		speed.setPaintTicks(false);
		tools.add(speed);

		// SET UP SIZE LABEL
		sizeL.setHorizontalAlignment(JLabel.LEFT);
		sizeL.setBounds(10, 275, 50, 25);
		tools.add(sizeL);

		// SET UP LEN LABEL
		lenL.setHorizontalAlignment(JLabel.LEFT);
		lenL.setBounds(140, 275, 50, 25);
		tools.add(lenL);

		// SET UP SIZE SLIDER
		size.setMajorTickSpacing(10);
		size.setBounds(55, 275, 75, 25);
		size.setPaintTicks(false);
		tools.add(size);

		// SET UP INFO AREA
		information.setBounds(10, 400, 160, 125);
		information.setEditable(false);
		tools.add(information);

		// SET UP CREDIT BUTTON
		credit.setBounds(40, 540, 100, 25);
		tools.add(credit);

		// ADD ACTION LISTENERS
		alg.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				VisualSorting.curAlg=alg.getSelectedIndex();
				information.setText(algInfo[curAlg]);
				if(alg.getSelectedIndex()==2) {
					VisualSorting.checkMerge=1;
					VisualSorting.checkBucket=0;
					VisualSorting.Update();
				}
				else if(alg.getSelectedIndex()==3)
				{
					VisualSorting.checkBucket=1;
					VisualSorting.checkMerge=0;
					VisualSorting.Update();
				}
				else
				{
					VisualSorting.checkBucket=0;
					VisualSorting.checkMerge=0;
					VisualSorting.Update();
					
				}
			}

		});
		graph.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				// canvas.setType(graph.getSelectedIndex());
				VisualSorting.type = graph.getSelectedIndex();
				VisualSorting.shuffleList();
				VisualSorting.reset();
				VisualSorting.Update();
			}
		});
		sort.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (VisualSorting.shuffled) {
					VisualSorting.sorting=true;
				}

			}
		});
		shuffle.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				VisualSorting.shuffleList();
				MainInterface.checklist.clear();
				VisualSorting.reset();
				VisualSorting.Update();
			}
		});
		speed.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				VisualSorting.spd = (int) speed.getValue()*10;
				msL.setText(VisualSorting.spd + " ms");
			}
		});
		size.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int val = size.getValue();
				if (size.getValue() == 5)
					val = 4;
				VisualSorting.len=val*10;
				lenL.setText(VisualSorting.len + "");
				if (VisualSorting.list.length !=VisualSorting.len)
					VisualSorting.shuffleList();
				VisualSorting.reset();
			}

		});
		credit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,
						"	                         Visual Sort\n" + "             Copyright (c) 2019-2020\n"
								+ "                        hoangbkcntt98\n"
								+ "           Build Date:  April 15, 2019   ",
						"Credit", JOptionPane.PLAIN_MESSAGE, new ImageIcon(""));
			}
		});

	}
	
}
