import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

public class VisualSorting {
	// MAIN FRAME
	private JFrame jf;
	// SUB CLASS
	SortingAlgorithms algorithm;// CLASS WHICH IMPLEMENT ALGORITHMS
	static MainInterface canvas; // CLASS CONTROL MAININTERFACE
	Tools tools; // CONTAINT FUNCTION OF APP
	// GENERAL ALGORITHMS VARIABLES
	public static int len=10;
	public static int curAlg = 0;
	public static int spd = 15;
	// MAIN INTERFACE VARIABLES
	public final static int SIZE = 600;
	public static int current = -1;
	public static int check = -1;
	public static int width = SIZE / (2 * len);
	public static int type = 0;
	public static int swap = -1;
	public static int swap1 = -1;
	public static int checkMerge = 0;
	public static int checkBucket = 0;
	
	// ARRAYS
	public static int[] list;
	public static ArrayList<Integer> d = new ArrayList<>();
	// BOOLEANS
	public static boolean sorting = false;// START SORTING ??
	public static boolean shuffled = true; // START SHUFFLED
	// CONSTRUCTOR
	public VisualSorting() {
		shuffleList(); // CREATE THE LIST
		initialize(); // INITIALIZE THE GUI
	}

	public static void createList() {
		list = new int[len]; // CREATES A LIST EQUAL TO THE LENGTH
		for (int i = 0; i < len; i++) { // FILLS THE LIST FROM 1-LEN
			list[i] = i + 1;
		}
	}

	public static void shuffleList() {
		Random r = new Random();
		createList();
		for (int a = 0; a < 500; a++) { // SHUFFLE RUNS 500 TIMES
			for (int i = 0; i < len; i++) { // ACCESS EACH ELEMENT OF THE LIST
				int rand = r.nextInt(100); // PICK A RANDOM NUM FROM 0-LEN
				list[i]= rand;
			}
		}
		sorting = false;
		shuffled = true;
	}

	public void initialize() {
		// SET UP FRAME
		jf = new JFrame();
		jf.setSize(jf.getMaximumSize());
		jf.setTitle("Visual Sort");
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setResizable(true);
		jf.setLocationRelativeTo(null);
		//jf.setLocation(0, 0);
		jf.getContentPane().setLayout(null);
		// BORDER STYLE
		Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		// SETUP TOOLS
		
		tools = new Tools();
		tools.setBorder(BorderFactory.createTitledBorder(loweredetched, "Controls"));
		tools.setBounds(0, 0, 190, SIZE + 120);
		tools.setBorder(BorderFactory.createLineBorder(Color.black));
		// SET UP CANVAS FOR GRAPH
		canvas = new MainInterface();
		canvas.setBounds(170, 0, 1320, SIZE + 120);
		canvas.setBorder(BorderFactory.createLineBorder(Color.black));
		
		jf.getContentPane().add(canvas);
		jf.getContentPane().add(tools);
		sorting();
	}


	// SORTING STATE
	public void sorting() {
		algorithm = new SortingAlgorithms(this);
		algorithm.setList(list);
		if (sorting == true) {
			try {
				switch (curAlg) { // CURRENT ALGORITHM IS EXECUTED
				case 0:
					algorithm.selectionSort();
					break;
				case 1:
					algorithm.bubbleSort();
					break;
				case 2:
					algorithm.mergeSort(0, len - 1);
					break;
				case 3:
					algorithm.bucketSort();
					break;
				default:

					break;
				}

			} catch (IndexOutOfBoundsException e) {
			} // EXCEPTION HANDLER INCASE LIST ACCESS IS OUT OF BOUNDS
		}
		reset(); // RESET
		pause(); // GO INTO PAUSE STATE
	}

	// PAUSE STATE
	public void pause() {
		int i = 0;
		while (!sorting) { // LOOP RUNS UNTIL SORTING STARTS
			i++;
			if (i > 100)
				i = 0;
			try {
				Thread.sleep(1);
			} catch (Exception e) {
			}
		}
		sorting(); // EXIT THE LOOP AND START SORTING
	}
	// RESET SOME VARIABLES
	public static void reset() {
		sorting = false;
		current = -1;
		check = -1;
		swap = -1;
		swap1= -1;
		Update();
	}

	// DELAY METHOD
	public static void delay() {
		try {
			Thread.sleep(spd);
		} catch (Exception e) {
		}
	}
	public static void delay(int c) {
		try {
			Thread.sleep(c);
		} catch (Exception e) {
		}
	}

	// UPDATES THE GRAPH AND LABELS
	public static void Update() {
		width = SIZE / len;
		canvas.repaint();
	}
	public static void Update(int height)
	{
		width = SIZE / len;
		canvas.repaint(0, SIZE-(height)*width, 1320, SIZE + 120);
	}
	public static void Update(int height,int width)
	{
		width = SIZE / len;
		canvas.repaint(0, SIZE-(height)*width, 1320,width);
	}
	public static void update(int height,int Height)
	{
		width = SIZE / len;
		canvas.repaint(0, height, 1320,Height);
		
	}
	
	
	// MAIN METHOD
	public static void main(String[] args) {

		new VisualSorting();
	}
}

