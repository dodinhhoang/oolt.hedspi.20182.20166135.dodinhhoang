import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.JPanel;
import javax.swing.Timer;


class MainInterface extends JPanel {
	
	private VisualSorting jf;
	private BufferedImage bg;
	public static int height =7;
	private ArrayList<TileUp> tileup = new ArrayList<>();
	public static ArrayList<Integer> checklist = new ArrayList<>();
	public static int checkM;
	public static int startB=0;
	public static LinkedList<LinkedList<Integer>> temp = new LinkedList<LinkedList<Integer>>();
	public static LinkedList<LinkedList<Integer>> allBuckets = new LinkedList<LinkedList<Integer>>();
	public static int SwapX1;
	public static int SwapX;
	public static int moveBx;
	public static int moveBy;
	public static int checkB1;
	public static int checkB2;
	public static int choiceB=-1;
	public MainInterface() {
		
		
	}
	// PAINTS THE GRAPH
	public int getIndex(ArrayList<Integer> templist,int count)
	{
		int i=0;
		int temp=0;
		for(int s:templist)
		{
			if(s==-1) temp--;
			if(temp==count) break;
			temp++;
			i++;
		}
		return i;
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//GET DATA FROM JF
		int len = jf.len;
		int width = jf.width;
		int list[] = jf.list;
		int SIZE = jf.SIZE;
		int current = jf.current;
		int check = jf.check;
		int type = jf.type;
		int swap = jf.swap;
		int swap1 = jf.swap1;
		ArrayList<Integer> d = jf.d;
		int checkMerge = jf.checkMerge;
		int checkBucket = jf.checkBucket;
		BackGround bg = new BackGround(0, 0, getWidth(), 795, this);
		TileUp element;
		if(checkMerge!=1&&checkBucket!=1)
		{
			
			// DRAW BACKGROUND
			bg.paint(g);
			for (int i = len; i < 2 * len; i++) {
				
				new Tile(i * width, SIZE + width, width, width, this).paint(g);// PAINT TILE
				if(i%2==1)
				{
					new TileEmpty(i * width,SIZE-5*width, width, width, this).paint(g);
				}
			}
			for (int i = 0; i < len; i++) { // RUNS TROUGH EACH ELEMENT OF THE LIST
				
				// BAR GRAPH TYPE
				element = new TileUp(i *width*2,SIZE-5*width, width, width, this,list[i]);
				element.paint(g);
				if (current > -1 && i == current) {
					new Character(type, i * width * 2, SIZE, width, width, this).paint(g);// PAINT MARIO OR SOGOKU
					element =new TileUp(i *width*2 + (int)width/20,SIZE-5*width, width, width, this,list[i]);
					element.paintRed(g);																			
				}
				
				if (check > -1 && i == check && i!=swap) {
					element = new TileUp(i *width*2 + (int)width/20,SIZE-5*width, width, width, this,list[i]);	
					element.paintGray(g);
				}
				if(swap >-1 && i== swap)
				{
					element = new TileUp(SwapX,SIZE-6*width, width, width, this,list[i]);	
					element.paintSwap(g);
					
				}
				
				if(swap1 >-1 && i== swap1)
				{
					element = new TileUp(SwapX1,SIZE-6*width, width, width, this,list[i]);	
					element.paintSwap(g);
					new PaintDes((1000-len*width)/2,200,this,list[swap],list[swap1],width).paintSwap(g);;
				}
				if(d.contains(i))
				{
					element = new TileUp(i *width*2 + (int)width/20,SIZE-5*width, width, width, this,list[i]);	
					element.paintSwap(g);
				}
				tileup.add(element);
				new Tile(i * width, SIZE + width, width, width, this).paint(g);// PAINT TILE
				if(i%2==1)
				{
					new TileEmpty(i * width,SIZE-5*width, width, width, this).paint(g);
				}
			}
			
		}
		
		if(checkMerge==1)
		{
			int i=0;
			ArrayList<Integer> templist = new ArrayList<Integer>();
			while(i<len)
			{
				if(d.contains(i))
				{
					templist.add(-1);
				}
				templist.add(list[i]);
				i++;
			}
			bg.paint(g);
			for (int j = 0; j < 2 * len; j++) {
				
				new Tile(j * width, SIZE + width, width, width, this).paint(g);// PAINT TILE
				
			}
			i=0;
			int count=0;
			for(int s:templist)
			{
				
				if(s!=-1)
				{
					element = new TileUp(i *width+(int)width/20,SIZE-8*width, width, width, this,s);
					element.paint(g);
				}else count--;
				if(checklist.contains(count)&&s!=-1)
				{					
					element = new TileUp(i *width+(int)width/20,SIZE-8*width, width, width, this,s);
					element.paintGray(g);
				}
				if(s!=-1&&count==current)
				{
					element = new TileUp(i *width+(int)width/20,SIZE-8*width, width, width, this,s);
					element.paintRed(g);
				}
				count++;
				i++;
			}
			i=0;
			count=0;

			if(temp.size()!=0)
			{
				for(int j=0;j<list.length;j++)
				{
					if(temp.get(j).size()!=0)
					{
						element = new TileUp(getIndex(templist, j)*width+(int)width/20,SIZE-6*width, width, width, this,temp.get(j).get(1));
						element.paint(g);
					}
					
				}
				
			}
			
		}
		if(checkBucket==1)
		{
			bg.paint(g);
			for (int j = 0; j < 2 * len; j++) {
				
				new Tile(j * width, SIZE + width, width, width, this).paint(g);// PAINT TILE
				
			}
			for(int i=0;i<len;i++)
			{
				element = new TileUp((int)i *width*3/2+(1000-len*width)/2,200, width, width, this,list[i]);
				element.paint(g);
				if (current > -1 && i == current) {
					//new Character(type, i * width * 2, SIZE, width, width, this).paint(g);// PAINT MARIO OR SOGOKU
					element = new TileUp((int)i *width*3/2+(1000-len*width)/2,200, width, width, this,list[i]);
					element.paintRed(g);
																						
				}
				
				if (check > -1 && i == check && i!=swap) {
					element = new TileUp((int)i *width*3/2+(1000-len*width)/2,200, width, width, this,list[i]);
					element.paintGray(g);
				}
				if(choiceB>-1&& i==choiceB)
				{
					element = new TileUp(moveBx,moveBy, width, width, this,list[i]);
					element.paintSwap(g);
				}
				if(swap >-1 && i== swap)
				{
					element = new TileUp(SwapX,200-width, width, width, this,list[i]);
					element.paintSwap(g);
					
				}
				
				if(swap1 >-1 && i== swap1)
				{
					element = new TileUp(SwapX1,200-width, width, width, this,list[i]);
					element.paintSwap(g);
				}
				
			}
			//DRAW BUCKET
			for(int i=0;i<10;i++)
			{
				element = new TileUp((int)i *width*3/2+(1200-10*width)/2,SIZE, width, width, this,i);
				element.paintRed(g);
				if(startB==1&&allBuckets.get(i)!=null)
				{
					int temph=SIZE-width;
					for(int j=0;j<allBuckets.get(i).size();j++)
					{
						element = new TileUp((int)i *width*3/2+(1200-10*width)/2,temph+(int)width/20, width, width, this,allBuckets.get(i).get(j));	
						if(i==checkB1&&j==checkB2)
						{
							element.paintGray(g);
						}else
						{
							element.paint(g);
						}
						temph=temph-width;
					}
					
					temph=SIZE-width;
				}
			}
		}
		
	}
	

	
}