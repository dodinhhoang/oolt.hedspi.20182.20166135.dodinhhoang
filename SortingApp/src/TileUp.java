
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Timer;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class TileUp extends Rectangle implements ActionListener{
	private BufferedImage bf;
	private JPanel jf;
	private int i;
	VisualSorting vs;
	Timer tm;
	int velX =2;
	public TileUp(int x, int y, int width, int height, JPanel jf, int i) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.jf = jf;
		this.i = i;
	}

	public void paint(Graphics g) {
		Font monoFont = new Font("Monospaced", Font.BOLD | Font.ITALIC, width - (int) width / 3);
		try {
			bf = ImageIO.read(new File("tilerong.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		g.drawImage(bf, this.x, this.y, this.width, this.height,jf);
		
		g.setColor(Color.black);
		g.setFont(monoFont);
		if(i!=-1)
		g.drawString(String.valueOf(i), this.x + (int) this.width / 6, this.y + this.width - (int) this.width / 3);
	}
	public void paintString(Graphics g)
	{
		Font monoFont = new Font("Monospaced", Font.BOLD | Font.ITALIC, width - (int) width / 3);
		g.setColor(Color.white);
		g.setFont(monoFont);
		g.drawString("Swap" , this.x + width, this.y + this.width );
		
	}
	public void paintRed(Graphics g) {
		Font monoFont = new Font("Monospaced", Font.BOLD | Font.ITALIC, width - (int) width / 3);
		try {
			bf = ImageIO.read(new File("tilehit.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		g.drawImage(bf, this.x, this.y, this.width, this.height, jf);
		
		g.setColor(Color.black);
		g.setFont(monoFont);
		g.drawString(String.valueOf(i), this.x + (int) this.width / 6, this.y + this.width - (int) this.width / 3);
	}

	public void paintGray(Graphics g) {
		Font monoFont = new Font("Monospaced", Font.BOLD | Font.ITALIC, width - (int) width / 3);
		try {
			bf = ImageIO.read(new File("tilecheck.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		g.drawImage(bf, this.x, this.y, this.width, this.height, jf);
		
		g.setColor(Color.black);
		g.setFont(monoFont);
		g.drawString(String.valueOf(i), this.x + (int) this.width / 6, this.y + this.width - (int) this.width / 3);
	}
	public void paintSwap(Graphics g) {
		Font monoFont = new Font("Monospaced", Font.BOLD | Font.ITALIC, width - (int) width / 3);
		try {
			bf = ImageIO.read(new File("tileswap.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		g.drawImage(bf, this.x, this.y, this.width, this.height, jf);
		
		g.setColor(Color.black);
		g.setFont(monoFont);
		
		g.drawString(String.valueOf(i), this.x + (int) this.width / 6, this.y + this.width - (int) this.width / 3);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
