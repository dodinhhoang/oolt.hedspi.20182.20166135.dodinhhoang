import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Character extends JPanel {

	private BufferedImage bf;
	private int x;
	private int y;
	private int width;
	private int height;
	private JPanel jf;
	private int type;
	public Character(int type,int x, int y, int width, int height, JPanel jf) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.jf = jf;
		this.type=type;
	}

	public void paint(Graphics g) {
		if(type==0)
		{
			try {
				bf = ImageIO.read(new File("marior.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			g.drawImage(bf, x, y, width, height, jf);
		}
		if(type ==1)
		{
			try {
				bf = ImageIO.read(new File("Goku.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			g.drawImage(bf, x, y, width, height, jf);
		}
	}
}
