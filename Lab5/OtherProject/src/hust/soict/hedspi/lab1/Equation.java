package hust.soict.hedspi.lab1;
import javax.swing.JOptionPane;
public class Equation {
	public static void main(String args[])
	{
		String a,b,c;
		double numA,numB,numC,result,delta,result1;
		//The first degree with one variable
		a = JOptionPane.showInputDialog(null,"Please input a:","ax+b=0",JOptionPane.INFORMATION_MESSAGE);
		b = JOptionPane.showInputDialog(null,"Please input b:","ax+b=0",JOptionPane.INFORMATION_MESSAGE);
		result = -Double.parseDouble(b)/Double.parseDouble(a);
		JOptionPane.showMessageDialog(null, "result :"+result);
		//The second degree with one variable
		a = JOptionPane.showInputDialog(null,"Please input a:","ax^2+bx+c=0",JOptionPane.INFORMATION_MESSAGE);
		b = JOptionPane.showInputDialog(null,"Please input b:","ax^2+bx+c=0",JOptionPane.INFORMATION_MESSAGE);
		c = JOptionPane.showInputDialog(null,"Please input c:","ax^2+bx+c=0",JOptionPane.INFORMATION_MESSAGE);
		numA = Double.parseDouble(a);
		numB = Double.parseDouble(b);
		numC = Double.parseDouble(c);
		delta = Math.pow(numB, 2)-4*numA*numC;
		if(delta ==0 )
		{
			result = -numB/(2*numA);
		}
		else if(delta >0)
		{
			result1 = (-numB+Math.sqrt(delta))/(2*numA);
			result = (-numB-Math.sqrt(delta))/(2*numA);
			JOptionPane.showMessageDialog(null, "result :x1="+result+" ,x2="+result1);
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Not result");
		}
		
		
		
	}
}
