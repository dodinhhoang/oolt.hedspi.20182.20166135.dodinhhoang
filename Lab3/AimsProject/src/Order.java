
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int  qtyOrdered;
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if(this.getQtyOrdered()==MAX_NUMBERS_ORDERED)
		{
			System.out.println("items of Order is full!!!\n Please remove anything");
		}else
		{
			
			itemsOrdered[qtyOrdered]=disc;
			this.setQtyOrdered(qtyOrdered+1);
			
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if(this.getQtyOrdered()==0)
		{
			System.out.println("The Order is empty!!");
			
		}else
		{
			this.setQtyOrdered(qtyOrdered-1);
			itemsOrdered[qtyOrdered]=null;
			
		}
	}
	public float totalCost()
	{
		float cost=0;
		for(int i=0;i<qtyOrdered;i++)
		{
			cost+=itemsOrdered[i].getCost();
		}
		return cost;
	}
	public static void main(String args[])
	{
		Order anOrder = new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Game of Throne");
		dvd2.setCategory("Drama");
		dvd2.setCost(19.05f);
		dvd2.setDirector("Roger Allers");
		dvd2.setLength(87);
		anOrder.addDigitalVideoDisc(dvd2);
		System.out.println("Total cost is:");
		System.out.println(anOrder.totalCost());
		
	}
	
}
