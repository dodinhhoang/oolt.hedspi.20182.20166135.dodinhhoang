import java.util.Scanner;

public class AddTwoMatrices {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [][][] array = new int[2][100][100];
		int [][] result = new int[100][100];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of rows:");
		int row = sc.nextInt();
		System.out.println("Enter number of column:");
		int col = sc.nextInt();
		for(int m=0;m<2;m++)
		{
			System.out.println("Enter value of matric "+ (m+1));
			
			for(int i=0;i<row;i++)
			{
				for(int j=0;j<col;j++)
				{
					System.out.format("Input value of matric%d[%d][%d]",m+1,i,j);
					array[m][i][j]=sc.nextInt();
				}
			}
			
		}
		System.out.println("Result:");
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<col;j++)
			{
				result[i][j]=array[0][i][j]+array[1][i][j];
				System.out.print(result[i][j]+" ");
				
			}
			System.out.println();
		}
		
	}

}
