import java.util.Scanner;

public class Sort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] arr = new int [100];
		int sum=0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Please input number of elements in array:");
		int n= sc.nextInt();
		for(int i=0;i<n;i++)
		{
			System.out.println("Please enter element "+i);
			arr[i]= sc.nextInt();
			sum+=arr[i];
		}
		System.out.println("The array you 've just entered:");
		for(int i=0;i<n;i++)
		{
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		int temp;
		for(int i=0;i<n;i++)
		{
			for(int j=i;j<n;j++)
			{
				if(arr[i]>arr[j])
				{
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
		System.out.println("The array after sorted:");
		for(int i=0;i<n;i++)
		{
			System.out.println(arr[i]+" ");
		}
		System.out.println("Sum of array is "+sum);
		double av = (double)sum/n;
		System.out.println("average of Array is :"+av);

	}

}
