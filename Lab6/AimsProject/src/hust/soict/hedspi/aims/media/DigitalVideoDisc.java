package hust.soict.hedspi.aims.media;

public class DigitalVideoDisc extends Media{
	
	private String director;
	private float length;
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public float getLength()
	{
		return this.length;
	}
	public DigitalVideoDisc(String title)
	{
		super(title);
	}
	
	public DigitalVideoDisc(String title,String category)
	{
		super(title,category);
	}
	public DigitalVideoDisc(String title, String category,float cost)
	{
		super(title,category,cost);
	}
	public DigitalVideoDisc(String title,String category,float cost,float length,String director)
	{
		super(title,category,cost);
		this.length = length;
		this.setDirector(director);

	}
	public boolean search(String str)
	{
		
		String []temp;
		int check=0,count =0;
		temp = str.split(" ");
		for(String s:temp)
		{
			if(this.getTitle().contains(s)==true) {
				check++;
			}
			count ++;
		}
		if(check == count) return true;
		return false;
	}
	
}
