package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media{
	private List<String> authors = new ArrayList<String>();
	public List<String> getAuthors() {
		return authors;
	}
	public Book(String title)
	{
		super(title);
	}
	public Book(String title, String category)
	{
		super(title,category);
	}
	public Book(String title,String category,float cost)
	{
		super(title,category,cost);
	}
	public Book(String title, String category,float cost,List<String> authors)
	{
		super(title,category,cost);
		this.authors= authors;
		
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public void addAuthor(String authorName)
	{
		int check=0;
		for(String s: authors)
		{
			if(s.equals(authorName))
			{
				check=1;
			}
		}
		if(check==1)
		{
			authors.add(authorName);
		}
		else
		{
			System.out.println("The author exist");
		}
	}
	public void removeAuthor(String authorName)
	{
		int check =0;
		for(String s: authors)
		{
			if(s.equals(authorName))
			{
				authors.remove(s);
				check =1;
			}
		}
		if(check==0)
		{
			System.out.println("Error!!!");
		}
		
	}
	
}
