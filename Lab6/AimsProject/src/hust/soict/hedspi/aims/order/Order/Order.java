package hust.soict.hedspi.aims.order.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.date.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITED_ORDERED = 5;
	private static int nbOrders = 0;
	private ArrayList<Media> itemsOrdered = new ArrayList<>();
	MyDate dateOrdered = new MyDate();

	public Order() {

	}

	public void addMedia(Media m) {
		if (itemsOrdered.contains(m)) {
			System.out.println("This media is exist");
		} else {
			itemsOrdered.add(m);
		}
	}

	public void removeMedia(int index) {
		int i = 0;
		itemsOrdered.remove(index);

	}

	public MyDate getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public float totalCost() {
		float cost = 0;
		for (Media m : itemsOrdered) {
			cost += m.getCost();
		}
		return cost;
	}

	public static void showMenu() {
		System.out.println("Order Management Application");
		System.out.println("------------------------------");
		System.out.println("1.Create new order");
		System.out.println("2.Add item to the order");
		System.out.println("3.Delete item by id");
		System.out.println("4.Display the items list of order");
		System.out.println("0.Exit");
		System.out.println("---------------------------------");
		System.out.println("Please choose a number : 0-1-2-3-4");

	}

	public void print() {
		int i = 0;
		Book m;
		DigitalVideoDisc disc;
		System.out.println("***************************************");
		System.out.println("              Order                    ");
		System.out.println("***************************************");
		System.out.print("Date: ");
		this.dateOrdered.print();
		System.out.println("Ordered Items:");
		if(itemsOrdered.size()==0)
		{
			System.out.println("Empty!!");
		}
		else
		{
			for (Media n : itemsOrdered) {
				if (n instanceof Book) {
					m = (Book) n;
					System.out.println((i + 1) + "  Book:" + m.getTitle() + "-" + m.getCategory() + "-" + m.getAuthors()+"-"+
							+ m.getCost() + "$");
				}
				else
				{
					disc = (DigitalVideoDisc)n;
					System.out.println((i + 1) + "  Disc:" + disc.getTitle() + "-" + disc.getCategory() + "-" + disc.getDirector()
					+"-"+ disc.getCost() + "$ - "+disc.getLength()+" s");
				}
				i++;
			}
			
		}
		
	}

	public static void main(String args[])
	{
		Order anOrder= new Order();
		Scanner sc = new Scanner(System.in);
		int choice;
		int choice1;
		String title,category,authorName,director;
		List<String> author = new ArrayList<>();
		float cost,length;
		while(true)
		{
			showMenu();
			choice = sc.nextInt();
			switch (choice) {
			case 1:
				anOrder.dateOrdered.accept();
				anOrder.dateOrdered.print();
				System.out.println("Order is created!!");
				break;
			case 2:
					System.out.println("What is kind of item?");
					System.out.println("1.Book");
					System.out.println("2.Disc");
					System.out.println("3.Exit");
					choice1 = sc.nextInt();
					switch(choice1)
					{
					case 1:
						sc.nextLine();
						System.out.println("Please input title:");
						title = sc.nextLine();
						
						System.out.println("Please input category:");
						
						category = sc.nextLine();
						System.out.println("Please input name of authors :");
						
						//System.out.println(book.getTitle()+book.getAuthors()+book.getCost()+book.getCategory());
						while(true)
						{
							
							authorName = sc.nextLine();						
							if(authorName.equals("stop"))
							{
								break;
							}else
							{
								author.add(authorName);
								
							}
							
						}
						System.out.println("Please input cost:");
						cost = sc.nextFloat();
						anOrder.addMedia(new Book(title,category,cost,author));
						System.out.println("OK!!");
						break;
					case 2:
						sc.nextLine();
						System.out.println("Please input title:");
						title = sc.nextLine();
						System.out.println("Please input category:");
						category = sc.nextLine();
						System.out.println("Please input director:");
						director = sc.nextLine();
						System.out.println("Please input length:");
						length = sc.nextFloat();
						System.out.println("Please input cost:");
						cost = sc.nextFloat();
						anOrder.addMedia(new DigitalVideoDisc(title,category,cost,length,director));
						System.out.println("OK!!");
						break;
						
					}
					break;
			case 3:
				System.out.println("Please input id of Item which you want to remove from order:");
				int index =sc.nextInt();
				if(index>anOrder.itemsOrdered.size())
				{
					System.out.println("index not available");
				}else
				{
					anOrder.removeMedia(index-1);
					System.out.println("Sucessfully!!");
				}
				break;
			case 4:
				anOrder.print();
				break;
			default:
				break;
			}
		}
	}

}
