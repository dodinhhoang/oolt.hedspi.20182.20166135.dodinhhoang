package hust.soict.hedspi.aims.order.Order;

import java.util.ArrayList;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.date.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITED_ORDERED = 5;
	private static int nbOrders = 0;
	public ArrayList<Media> itemsOrdered = new ArrayList<>();
	public MyDate dateOrdered = new MyDate();

	public Order() {

	}

	public void addMedia(Media m) {
		if (itemsOrdered.contains(m)) {
			System.out.println("This media is exist");
		} else {
			itemsOrdered.add(m);
		}
	}

	public void removeMedia(int index) {
		int i = 0;
		itemsOrdered.remove(index);

	}

	public MyDate getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public float totalCost() {
		float cost = 0;
		for (Media m : itemsOrdered) {
			cost += m.getCost();
		}
		return cost;
	}

	public void print() {
		int i = 0;
		Book m;
		DigitalVideoDisc disc;
		System.out.println("***************************************");
		System.out.println("              Order                    ");
		System.out.println("***************************************");
		System.out.print("Date: ");
		this.dateOrdered.print();
		System.out.println("Ordered Items:");
		if(itemsOrdered.size()==0)
		{
			System.out.println("Empty!!");
		}
		else
		{
			for (Media n : itemsOrdered) {
				if (n instanceof Book) {
					m = (Book) n;
					System.out.println((i + 1) + "  Book:" + m.getTitle() + "-" + m.getCategory() + "-" + m.getAuthors()+"-"+
							+ m.getCost() + "$");
				}
				else if(n instanceof DigitalVideoDisc)
				{
					disc = (DigitalVideoDisc)n;
					System.out.println((i + 1) + "  DVD:" + disc.getTitle() + "-" + disc.getCategory() + "-" + disc.getDirector()
					+"-"+ disc.getCost() + "$ - "+disc.getLength()+" s");
				}
				else 
				{
					CompactDisc cd = (CompactDisc) n;
					System.out.println((i + 1) + "  CD:" + cd.getTitle() + "-" + cd.getCategory() + "-" + cd.getDirector()
					+"-"+ cd.getCost() + "$ - "+cd.getLength()+" s");
					
				}
				i++;
			}
			
		}
		
	}

	
}
