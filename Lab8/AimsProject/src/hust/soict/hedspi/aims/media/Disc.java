package hust.soict.hedspi.aims.media;

public class Disc extends Media{
	private float length;
	private String director;
	public float getLength() {
		return length;
	}
	public void setLength(float length) {
		this.length = length;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public Disc()
	{
		
	}
	public Disc(String title,String category,String director,float length,float cost)
	{
		super(title,category,cost);
		this.director = director;
		this.length = length;
	}
	
	

}
