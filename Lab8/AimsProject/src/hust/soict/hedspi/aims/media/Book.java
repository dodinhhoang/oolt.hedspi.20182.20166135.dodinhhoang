package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Book extends Media implements Comparable{
	private List<String> authors = new ArrayList<String>();
	private String content;
	ArrayList<String> listword= new ArrayList<>();
	private List<String> contentTokens= new ArrayList<String>();
	private Map<String,Integer> wordFrequency = new TreeMap<>();
	public void processContent()
	{
		String [] temp = content.split(" ");
		int [] fr;
		for(String s:temp)
		{
			//System.out.println(s);
			contentTokens.add(s);
		}
		Collections.sort(contentTokens);
		HashSet<String> Tokens = new HashSet<>(contentTokens);
		listword= new ArrayList<>(Tokens);
		
		Collections.sort(listword);
		fr = new int[listword.size()];
		Arrays.fill(fr,0);
		for(int i=0;i<listword.size();i++)
		{
			for(int j=0;j<contentTokens.size();j++)
			{
				if(listword.get(i).equals(contentTokens.get(j)))
				{
					fr[i]++;
				}
			}
		}
		for(int i=0;i<listword.size();i++)
		{
			wordFrequency.put(listword.get(i),fr[i]);
		}
		//System.out.println("The set is: " + wordFrequency.entrySet());
	}
	@Override
	public String toString()
	{
		
		String contentS = "title -"+this.getTitle()+"  content:  "+this.content+" - "+"length:"+listword.size()+"- "+"The set is: " + wordFrequency.entrySet(); 
		return contentS;
	}
	public List<String> getAuthors() {
		return authors;
	}
	public Book(String content,String title,int check)
	{
		this.content=content;
		this.setTitle(title);
	}
	public Book(String title)
	{
		super(title);
	}
	public Book(String title, String category)
	{
		super(title,category);
	}
	public Book(String title,String category,float cost)
	{
		super(title,category,cost);
	}
	public Book(String title, String category,float cost,List<String> authors)
	{
		super(title,category,cost);
		this.authors= authors;
		
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public void addAuthor(String authorName)
	{
		int check=0;
		for(String s: authors)
		{
			if(s.equals(authorName))
			{
				check=1;
			}
		}
		if(check==1)
		{
			authors.add(authorName);
		}
		else
		{
			System.out.println("The author exist");
		}
	}
	public void removeAuthor(String authorName)
	{
		int check =0;
		for(String s: authors)
		{
			if(s.equals(authorName))
			{
				authors.remove(s);
				check =1;
			}
		}
		if(check==0)
		{
			System.out.println("Error!!!");
		}
		
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		
		return 0;
	}
	
}
