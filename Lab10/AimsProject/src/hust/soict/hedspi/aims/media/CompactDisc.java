package hust.soict.hedspi.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable,Comparable{
	private String artist;
	private float length;
	private ArrayList<Track> tracks= new ArrayList<>();
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public void addTrack(Track track)
	{
		if(!tracks.contains(track))
		{
			tracks.add(track);
			System.out.println("Sucessfully!!");
		}
		else
		{
			System.out.println("Track is existed !!");
		}
	}
	public void removeTrack(Track track)
	{
		if(tracks.contains(track))
		{
			tracks.remove(track);
			System.out.println("Sucessfully!!");
		}
		else
		{
			System.out.println("Track is not existed !!");
		}
	}
	public float getLength()
	{
		float totalLength=0;
		for(Track s:tracks)
		{
			totalLength+=s.getLength();
		}
		return totalLength;
	}
	
	public CompactDisc(String title,String category,String artist, float cost) {
		super();
		this.setCost(cost);
		this.setArtist(artist);
		this.setCategory(category);
		this.setTitle(title);
		this.artist = artist;
		
	}
	public CompactDisc(String title) {
		// TODO Auto-generated constructor stub
		this.setTitle(title);
	}
	@Override
	public void play() throws PlayerException{
		// TODO Auto-generated method stub
		if(this.getLength()<=0)
		{
			System.err.println("ERROR:CD length is 0");
			throw (new PlayerException());
		}
		for(Track t:tracks)
		{
			try
			{
				t.play();
			}catch(PlayerException e)
			{
				e.printStackTrace();
			}
			
		}
		
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		CompactDisc cd = (CompactDisc) o;
		if(this.tracks.size()<cd.tracks.size()) return -1;
		else if(this.tracks.size()>cd.tracks.size()) return 1;
		else
		{
			return this.getLength()<cd.getLength()?-1:1;
		}
	}
	

}
