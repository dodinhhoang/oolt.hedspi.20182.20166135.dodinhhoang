package hust.soict.hedspi.aims.media;

public class DigitalVideoDisc extends Disc implements Playable, Comparable {

	public boolean search(String str) {

		String[] temp;
		int check = 0, count = 0;
		temp = str.split(" ");
		for (String s : temp) {
			if (this.getTitle().contains(s) == true) {
				check++;
			}
			count++;
		}
		if (check == count)
			return true;
		return false;
	}

	public DigitalVideoDisc(String title) {
		super();
		this.setTitle(title);

	}
	public DigitalVideoDisc(String title,float cost)
	{
		
		this.setTitle(title);
		this.setCost(cost);
	}

	public DigitalVideoDisc(String title, String category, String director, float length, float cost) {
		super(title, category, director, length, cost);
	}

	@Override
	public void play() throws PlayerException {
		// TODO Auto-generated method stub
		if(this.getLength()<=0)
		{
			System.err.println("ERROR:DVD length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		DigitalVideoDisc disc = (DigitalVideoDisc) o;
		return Float.compare(this.getCost(),disc.getCost());
		//return this.getTitle().compareTo(disc.getTitle());
	}

}
