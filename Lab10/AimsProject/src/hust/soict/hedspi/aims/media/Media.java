package hust.soict.hedspi.aims.media;

public abstract class Media implements Comparable{
	private String title;
	private String category;
	private float cost;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public Media(String title) {
		super();
		this.title = title;
	}
	public Media(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}
	public Media(String title, String category, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	@Override
	public boolean equals(Object o)// is checked in CheckEquals()
	{
		Media m ;
		if(this.getClass().equals(o.getClass()))
		{
			m = (Media)o;
			return ((m.getCost()==this.getCost())&&(m.getTitle().equals(this.getTitle())))? true:false;
		}
		return false;
	}
	
	public Media() {
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		DigitalVideoDisc disc = (DigitalVideoDisc) o;
		return Float.compare(this.getCost(),disc.getCost());
		//return this.getTitle().compareTo(disc.getTitle());
	}
	
}
