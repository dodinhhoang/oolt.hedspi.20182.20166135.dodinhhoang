package hust.soict.hedspi.aims.media;

public class Track implements Playable,Comparable{
	private String title;
	private float length;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public float getLength() {
		return length;
	}
	public void setLength(float length) {
		this.length = length;
	}
	public Track()
	{
		
	}
	public Track(String title,float length)
	{
		this.setTitle(title);
		this.setLength(length);
	}
	@Override
	public void play() throws PlayerException{
		// TODO Auto-generated method stub
		if(this.getLength()<=0)
		{
			System.err.println("ERROR:DVD length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing Track:"+this.getTitle());
		System.out.println("Track length:"+this.getLength());
		
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
