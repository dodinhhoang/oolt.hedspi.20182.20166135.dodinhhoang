package hust.soict.hedspi.aims.Aims;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.hedspi.date.MyDate;

public class Student {
	private int studentID;
	private String studentName;
	private MyDate birthday;
	private Float gpa;
	public int getStudentID() {
		return studentID;
	}
	public void setStudentID(int studentID) {
		studentID = studentID;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public MyDate getBirthday() {
		return birthday;
	}
	public void setBirthday(MyDate birthday) {
		this.birthday = birthday;
	}
	public Float getGpa() {
		return gpa;
	}
	public void setGpa(Float gqa) {
		this.gpa = gpa;
	}
	public Student(int studentID, String studentName, MyDate birthday, Float gqa) {
		super();
		this.studentID = studentID;
		this.studentName = studentName;
		this.birthday = birthday;
		this.gpa = gqa;
	}
	public void print()
	{
		System.out.println(this.studentID);
		System.out.println(this.studentName);
		birthday.print();
		System.out.println(this.gpa);
	}
	public static void main(String args[]) throws IllegalBirthDayException, IllegalGPAException
	{
		List<Student> students = new ArrayList<Student>();
		Student temp;
		int studentID;
		String studentName;
		MyDate birthday= new MyDate();
		Float gpa;
		Scanner sc = new Scanner(System.in);
		
		while(true)
		{
			//sc.nextLine();
			System.out.println("Please input id of student:");
			studentID =sc.nextInt();
			sc.nextLine();
			if(studentID == 0)
			{
				break;
			}
			System.out.println("Please input name of student:");
			studentName = sc.nextLine();
			
			System.out.println("Please input birthday(eg:February 1st 2019or 1/2/2019)");
			birthday.accept();
			if(birthday.getDay()<0||birthday.getDay()>31||birthday.getMonth()>12)
			{
				System.err.println("Day is not available");
				throw(new IllegalBirthDayException());
			}
			System.out.println("Please input GPA");
			gpa = sc.nextFloat();
			if(gpa<0||gpa>10)
			{
				System.err.println("gpa should between 0 to 10");
				throw(new IllegalGPAException());
			}
			sc.nextLine();
			students.add(new Student(studentID,studentName,birthday,gpa));
		
		}
		System.out.println("Student information");
		System.out.println("-----------------------------------");
		for(int i=0;i<students.size();i++)
		{
			students.get(i).print();
		}
	}
	
}
