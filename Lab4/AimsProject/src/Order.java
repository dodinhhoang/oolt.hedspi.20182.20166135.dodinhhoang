
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITED_ORDERED =5;
	private static int nbOrders =0;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int  qtyOrdered;
	MyDate dateOrdered = new MyDate();
	public Order()
	{
		if(nbOrders == MAX_LIMITED_ORDERED)
		{
			System.out.println("Order is full");
		}
		else
		nbOrders ++;
	}
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public MyDate getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if(this.getQtyOrdered()==MAX_NUMBERS_ORDERED)
		{
			System.out.println("items of Order is full!!!\n Please remove anything");
		}else
		{
			
			itemsOrdered[qtyOrdered]=disc;
			this.setQtyOrdered(qtyOrdered+1);
			
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc1, DigitalVideoDisc disc2)
	{
		if(this.getQtyOrdered()+1==MAX_NUMBERS_ORDERED)
		{
			System.out.println("Items of Order is full!!!\n Please remove anything");
		}else
		{
			itemsOrdered[qtyOrdered]=disc1;
			itemsOrdered[qtyOrdered+1]=disc2;
			this.setQtyOrdered(qtyOrdered+2);
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if(this.getQtyOrdered()==0)
		{
			System.out.println("The Order is empty!!");
			
		}else
		{
			this.setQtyOrdered(qtyOrdered-1);
			itemsOrdered[qtyOrdered]=null;
			
		}
	}
	public float totalCost()
	{
		float cost=0;
		for(int i=0;i<qtyOrdered;i++)
		{
			cost+=itemsOrdered[i].getCost();
		}
		return cost;
	}
	public void print()
	{
		System.out.println("***************************************");
		System.out.println("              Order                    ");
		System.out.println("***************************************");
		System.out.print("Date: ");
		this.dateOrdered.print();
		System.out.println("Ordered Items:");
		for(int i=0;i<qtyOrdered;i++)
		{
			System.out.println((i+1)+".DVD-"+itemsOrdered[i].getTitle()+"-"+itemsOrdered[i].getDirector()+"-"+itemsOrdered[i].getLength()+":"+itemsOrdered[i].getCost()+"$");
		}
		
	}
	public static void main(String args[])
	{
		Order anOrder = new Order();
		anOrder.dateOrdered.accept();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Game of Throne");
		dvd2.setCategory("Drama");
		dvd2.setCost(19.05f);
		dvd2.setDirector("Roger Allers");
		dvd2.setLength(87);
		anOrder.addDigitalVideoDisc(dvd2);
		anOrder.print();
		System.out.println("Total cost is:");
		System.out.println(anOrder.totalCost());
		
		
	}
	
}
