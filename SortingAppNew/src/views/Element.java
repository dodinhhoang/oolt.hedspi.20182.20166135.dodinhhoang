package views;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import models.Data;
public class Element{
	private BufferedImage bf;
	private JPanel jf;
	private int x;
	private int y;
	private int value;
	private int width;
	private int height;
	public Element(int x, int y, int width, int height, JPanel jf, int i) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.jf = jf;
		this.value = i;
	}
	public void paint(Graphics g,String str) {
		if(Data.curAlg==2&&value==-2) return;
		Font monoFont = new Font("Monospaced", Font.BOLD | Font.ITALIC, width - (int) width / 3);
		try {
			bf = ImageIO.read(new File(str));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		g.drawImage(bf, this.x, this.y, this.width, this.height,jf);
		
		g.setColor(Color.black);
		g.setFont(monoFont);
		if(value!=-1)
		g.drawString(String.valueOf(value), this.x + (int) this.width / 6, this.y + this.width - (int) this.width / 3);
	}
	public void setY(int y)
	{
		this.y=y;
	}
	public void setX(int x)
	{
		this.x=x;
	}
	public int getX()
	{
		return this.x;
	}
	public int getValue()
	{
		return this.value;
	}
	public void Right()
	{
		this.x++;
	}
	public void Left()
	{
		this.x--;
	}
	public void Up()
	{
		this.y--;
	}
	public void Down()
	{
		this.y++;
	}

}
