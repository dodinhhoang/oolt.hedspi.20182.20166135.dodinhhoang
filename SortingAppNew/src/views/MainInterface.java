package views;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Stack;

import javax.swing.JPanel;

import models.Data;

public class MainInterface extends JPanel {
	private ArrayList<Element> elements;
	private ArrayList<Element> tempElement = new ArrayList<Element>();
	private ArrayList<Integer> templist;
	public ArrayList<Integer> tempD = new ArrayList<>(); 
	public ArrayList<Integer> checklist = new ArrayList<Integer>();
	public ArrayList<ArrayList<Element>> allBuckets = new ArrayList<ArrayList<Element>>();
	public Stack sort = new Stack();
	private int width;
	Thread thread;
	public void initMergeSort()
	{
		width = Data.WIDTH / (2*Data.len);
		templist = new ArrayList<Integer>();
		for (int i = 0; i < Data.len; i++) {
			if (tempD.contains(i)) {
				templist.add(-2);
			}
			templist.add(Data.list[i]);
		}
		for (int i = 0; i < templist.size(); i++) {
			Element e = new Element(i * width + (1200-Data.len*width)/2, 200, width, width, this, templist.get(i));
			elements.add(e);
		}
	}
	public void initNormalSort()
	{
		for (int i = 0; i < Data.len; i++) {
			Element e = new Element(i * width + (1200-Data.len*width)/2, 200, width, width, this, Data.list[i]);
			elements.add(e);
		}

	}
	public void initBucketSort()
	{
		for (int i = 0; i < Data.len; i++) {
			Element e = new Element(i * width + (1200-Data.len*width)/2, 100+width, width, width, this, Data.list[i]);
			elements.add(e);
		}
		//SET UP BUCKET;
		for (int i = 0; i < 10; i++) {
			allBuckets.add(new ArrayList<Element>());
			

		}
		
	}
	public void init() {
		elements = new ArrayList<>();
		width = Data.WIDTH / (3*Data.len/2);
		if (Data.curAlg < 2) {
			initNormalSort();
		}
		if (Data.curAlg == 2) {
			initMergeSort();
		}
		if(Data.curAlg==3)
		{
			initBucketSort();
		}

	}
	public void paintElements(Graphics g, ArrayList<Element> elements) {

		for (int i = 0; i < elements.size(); i++) {
			if (i == Data.swap || i == Data.swap1) {

				elements.get(i).paint(g, "./img/tileswap.png");

			} else if (i == Data.check || checklist.contains(i)) {
				if (!elements.equals(tempElement))
					elements.get(i).paint(g, "./img/tilecheck.png");
				else {
					elements.get(i).paint(g, "./img/tilerong.png");

				}
			} else if (i == Data.current) {
				elements.get(i).paint(g, "./img/tilehit.png");
			} else
				elements.get(i).paint(g, "./img/tilerong.png");
		}
	}
	@Override
	public void paint(Graphics g) {
		new BackGround(0, 0, 1300, 700, this).paint(g);
		if (Data.curAlg < 2)
			paintElements(g, elements);
		if (Data.curAlg == 2) {
			paintElements(g, elements);
			if (tempElement.size() != 0) {
				paintElements(g, tempElement);
			}
		}
		if(Data.curAlg==3)
		{
			paintElements(g, elements);
			for(int i=0;i<10;i++)
			{
				new Element((int)i *width*4/3+(1200-(10*4/3)*width)/2, 700-width, width, width, this,i).paint(g,"./img/tilehit.png");;
			}
			if(allBuckets.size()!=0)
			{
				for(int i=0;i<10;i++)
				{
					if(allBuckets.get(i).size()!=0)
					paintElements(g, allBuckets.get(i));
				}
			}
		}
	}
	public MainInterface() {
		init();
	}
	public void swap(int i, int j) {
		Data.swap = i;
		Data.swap1 = j;
		int swapW = Data.swap * width + 20;
		int swapW1 = Data.swap1 * width + 20;
		for (int k = 0; k < width; k++) {
			elements.get(Data.swap).Up();
			elements.get(Data.swap1).Up();
			repaint();
			sleep();
		}
		while (swapW < swapW1) {
			elements.get(Data.swap1).Left();
			elements.get(Data.swap).Right();
			repaint();
			sleep();
			swapW++;
		}
		for (int k = 0; k < width; k++) {
			elements.get(Data.swap).Down();
			elements.get(Data.swap1).Down();
			repaint();
			sleep();
		}
		Data.swap = -1;
		Data.swap1 = -1;
		Update();
	}

	public void setCurrent(int c) {
		Data.current = c;
		Update();
	}

	public void resetM() {
		tempD.remove(tempD.size() - 1);
		tempElement.clear();
		templist.clear();
		checklist.clear();
		Update();
	}

	public int getTemplistIndex(int index) {
		int count = 0, i = 0;
		for (int s : templist) {
			if (s == -2)
				count--;
			if (index == count)
				break;
			i++;
			count++;
		}
		return i;
	}
	public void addToBucket(int j,int i)
	{
		Element e = new Element((int)j *width*4/3+(1200-(10*4/3)*width)/2, 700-(2+allBuckets.get(j).size()) * width, width, width, this, elements.get(i).getValue());
		allBuckets.get(j).add(e);
		Update();
	}
	public void removeBucket(int k,Element e)
	{
		allBuckets.get(k).remove(e);
		for(int j=0;j<allBuckets.get(k).size();j++)
		{
			for(int i=0;i<width;i++)
			{
				allBuckets.get(k).get(j).Down();
				repaint();
				sleep();
			}
		}
		Update();
	}
	public void addToTempElements(int index, int index1) {
		System.out.println(tempElement.size());
		if (tempElement.size() == 0) {
			Element e = new Element(index * width + (1200-Data.len*width)/2, 200 + 2 * width, width, width, this, templist.get(index1));
			tempElement.add(e);
		} else {
			Element e = new Element(tempElement.get(tempElement.size() - 1).getX() + width, 200 + 2 * width, width,
					width, this, templist.get(index1));
			tempElement.add(e);
		}
		repaint();
		sleep(Data.spd);
		//System.out.println(templist.get(index1) + " index " + index1);
	}

	public void setCheck(int check) {
		Data.check = check;
		repaint();
		sleep(1);
		Data.check = -1;
	}

	public void Update() {
		init();
		repaint();
		sleep(Data.spd);
	}
	public void UpdateTools()
	{
		init();
		repaint();
		sleep(0);
	}
	public void sleep() {
		try {
			thread.sleep(Data.spd);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void sleep(int spd) {
		try {
			thread.sleep(spd * 100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
