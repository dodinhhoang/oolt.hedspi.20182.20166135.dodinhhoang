package alogorithms;

import models.Data;
import views.MainInterface;

public class BubbleSort extends Sort{

	public BubbleSort(MainInterface canvas) {
		super(canvas);
		// TODO Auto-generated constructor stub
	}
	public void start()
	{
		int len = this.getLen();
		int list[] = this.getList();
		MainInterface canvas = this.getCanvas();
		int c = 1;
		boolean noswaps = false;
		while (!noswaps && Data.sorting) {
			canvas.setCurrent(len - c);
			noswaps = true;
			for (int i = 0; i < len - c; i++) {
				if (!Data.sorting)
					break;
				if (list[i + 1] < list[i]) {

					noswaps = false;
					swap(i + 1, i);
					canvas.swap(i, i + 1);
				}
				canvas.setCheck(i + 1);
			}
			c++;
			if(Data.step!=0)
			{
				Data.sorting=false;
				Data.pause();
			}
		}
		Data.reset();
		canvas.Update();
	}

}
