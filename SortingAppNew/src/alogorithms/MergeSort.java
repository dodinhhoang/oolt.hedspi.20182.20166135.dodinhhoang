package alogorithms;

import java.util.ArrayList;

import models.Data;
import views.MainInterface;

public class MergeSort extends Sort{
	public MergeSort(MainInterface canvas) {
		super(canvas);
		// TODO Auto-generated constructor stub
	}
	void merge(int l, int m, int r) {
		MainInterface canvas = this.getCanvas();
		int list[]= this.getList();
		int n1 = m - l + 1;
		int n2 = r - m;
		int L[] = new int[n1];
		int R[] = new int[n2];
		for (int i = 0; i < n1; i++) {
			if (!Data.sorting)
				break;
			L[i] = list[l + i];
			canvas.checklist.add(canvas.getTemplistIndex(l+i));	
		}
		for (int j = 0; j < n2; j++) {
			if (!Data.sorting)
				break;
			R[j] = list[m + 1 + j];
			canvas.checklist.add(canvas.getTemplistIndex(m+1+j));	
		}
		canvas.Update();
		int i = 0, j = 0;
		int k = l;
		while (i < n1 && j < n2 && Data.sorting) {
			if (L[i] <= R[j]) {
				list[k] = L[i];
				canvas.addToTempElements(canvas.getTemplistIndex(k),canvas.getTemplistIndex(l+i));
				i++;
			} else {
				
				list[k] = R[j];
				canvas.addToTempElements(canvas.getTemplistIndex(k),canvas.getTemplistIndex(m+1+j));
				j++;
			}
			k++;
		}
		while (i < n1 && Data.sorting) {
			
			list[k] = L[i];	
			canvas.addToTempElements(canvas.getTemplistIndex(k),canvas.getTemplistIndex(l+i));
			i++;
			k++;
		}
		while (j < n2 && Data.sorting) {
			
			list[k] = R[j];
			canvas.addToTempElements(canvas.getTemplistIndex(k),canvas.getTemplistIndex(m+1+j));
			j++;
			k++;
		}
		canvas.resetM();
		if(Data.step!=0)
		{
			Data.sorting=false;
			Data.pause();
		}

	}
	public void start(int l,int r)
	{
		MainInterface canvas = this.getCanvas();
		if (l < r && Data.sorting == true) {
			int m = (l + r) / 2;
			addToTempD(m + 1,canvas.tempD);
			if(Data.step!=0)
			{
				Data.sorting=false;
				Data.pause();
			}
			start(l, m);
			start(m + 1, r);
			merge(l, m, r);
		}
	}
	public void addToTempD(int d,ArrayList<Integer> tempD)
	{
		tempD.add(d);
		this.getCanvas().Update();
	}
	

}
