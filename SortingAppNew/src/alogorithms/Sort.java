package alogorithms;



import models.Data;
import views.MainInterface;

abstract class Sort {
	private MainInterface canvas;
	private int [] list ;
	private int len;
	Sort(MainInterface canvas)
	{
		this.canvas = canvas;
		this.list = Data.list;
		this.len = Data.len;
	}
	public void swap(int i1, int i2) {
		int temp = list[i1];
		list[i1] = list[i2];
		list[i2] = temp;
	}
	public void start() {}
	public MainInterface getCanvas() {
		return canvas;
	}
	public void setCanvas(MainInterface canvas) {
		this.canvas = canvas;
	}
	public int[] getList() {
		return list;
	}
	public void setList(int[] list) {
		this.list = list;
	}
	public int getLen() {
		return len;
	}
	public void setLen(int len) {
		this.len = len;
	};
	

}
