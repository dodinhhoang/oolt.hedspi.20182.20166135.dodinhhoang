package alogorithms;

import java.util.ArrayList;
import java.util.Arrays;

import models.Data;
import views.Element;
import views.MainInterface;

public class BucketSort extends Sort {

	public BucketSort(MainInterface canvas) {
		super(canvas);
		// TODO Auto-generated constructor stub
	}
	public void insertionSort(int start, int end) {
		int list[] = this.getList();
		MainInterface canvas = this.getCanvas();
		for (int i = start + 1; i <= end; i++) {
			canvas.setCurrent(i);
			int j = i;
			while (list[j] < list[j - 1] && Data.sorting) {
				swap(j, j - 1);
				canvas.swap(j, j-1);
				if (j != i) {
					canvas.setCheck(j);
				}
				if (j > start + 1)
					j--;
			}
			canvas.Update();
		}
	}
		
	public void start() {
		MainInterface canvas = this.getCanvas();
		int list[] = this.getList();
		int counter = 0;
		for (int i = 0; i < list.length; i++) {
			canvas.setCheck(i);
			int j = list[i] / 10;
			canvas.addToBucket(j, i);
			if (Data.step != 0) {
				Data.sorting = false;
				Data.pause();
			}
		}
		canvas.setCheck(-1);
		Arrays.fill(list, -1);
		canvas.Update();
		for (int i = 0; i < 10; i++) {
			System.out.println(canvas.allBuckets.get(i).size());
			ArrayList<Element> clone = (ArrayList<Element>) canvas.allBuckets.get(i).clone();
			for (int j = 0; j < clone.size(); j++) {
				list[counter] = clone.get(j).getValue();
				canvas.removeBucket(i, clone.get(j));
				counter++;
				if (Data.step != 0) {
					Data.sorting = false;
					Data.pause();
				}
			}
		}
		insertionSort(0, list.length - 1);
		canvas.allBuckets.clear();
		Data.reset();
		canvas.Update();
	}
}
