package alogorithms;

import models.Data;
import views.MainInterface;

public class SelectionSort extends Sort{
	public SelectionSort(MainInterface canvas) {
		super(canvas);
		// TODO Auto-generated constructor stub
	}
	public void start()
	{
		int len = this.getLen();
		int list[] = this.getList();
		MainInterface canvas = this.getCanvas();
		int c = 0;
		while (c < len && Data.sorting) {
			int sm = c;
			canvas.setCurrent(c);
			for (int i = c + 1; i < len; i++) {
				canvas.setCheck(i);
				if (!Data.sorting)
					break;
				if (list[i] < list[sm]) {
					sm = i;
				}
			}
			if (c != sm) {
				swap(c, sm);
				canvas.swap(c, sm);
			}
			c++;		
			if(Data.step!=0)
			{
				Data.sorting=false;
				Data.pause();
			}
		}
		Data.reset();
		canvas.Update();
		
	}

}
