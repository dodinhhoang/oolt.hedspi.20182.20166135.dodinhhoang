package controls;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import models.Attributes;
import views.Element;
import views.MainInterface;


public class SortingAlgorithms {
	private int list[];
	private int len;
	private MainInterface canvas;

	public SortingAlgorithms(MainInterface canvas) {
		this.canvas = canvas;
		this.list = Attributes.list;
		this.len = Attributes.len;
	}
	public void selectionSort() {
		int c = 0;
		while (c < len && Attributes.sorting) {
			canvas.sort.push(list.clone());
			int sm = c;
			canvas.setCurrent(c);
			for (int i = c + 1; i < len; i++) {
				canvas.setCheck(i);
				if (!Attributes.sorting)
					break;
				if (list[i] < list[sm]) {
					sm = i;
				}
			}
			if (c != sm) {
				swap(c, sm);
				canvas.swap(c, sm);
			}
			c++;		
			if(Attributes.step!=0)
			{
				
				Attributes.sorting=false;
				Attributes.pause();
				
			}
		}
		Attributes.reset();
		canvas.Update();
		
		

	}

	public void bubbleSort() {
		Attributes at = null;
		int c = 1;
		boolean noswaps = false;
		while (!noswaps && at.sorting) {
			canvas.setCurrent(len - c);
			noswaps = true;
			for (int i = 0; i < len - c; i++) {
				if (!at.sorting)
					break;
				if (list[i + 1] < list[i]) {

					noswaps = false;
					swap(i + 1, i);
					canvas.swap(i, i + 1);
				}
				canvas.setCheck(i + 1);
			}
			c++;
			if(Attributes.step!=0)
			{
				Attributes.sorting=false;
				Attributes.pause();
			}
		}
		Attributes.reset();
		canvas.Update();
	}

	void merge(int l, int m, int r) {
		int n1 = m - l + 1;
		int n2 = r - m;
		int L[] = new int[n1];
		int R[] = new int[n2];
		for (int i = 0; i < n1; i++) {
			if (!Attributes.sorting)
				break;
			L[i] = list[l + i];
			canvas.checklist.add(canvas.getTemplistIndex(l+i));	
		}
		for (int j = 0; j < n2; j++) {
			if (!Attributes.sorting)
				break;
			R[j] = list[m + 1 + j];
			canvas.checklist.add(canvas.getTemplistIndex(m+1+j));	
		}
		canvas.Update();
		int i = 0, j = 0;
		int k = l;
		while (i < n1 && j < n2 && Attributes.sorting) {
			if (L[i] <= R[j]) {
				list[k] = L[i];
				canvas.addToTempElements(canvas.getTemplistIndex(k),canvas.getTemplistIndex(l+i));
				i++;
			} else {
				
				list[k] = R[j];
				canvas.addToTempElements(canvas.getTemplistIndex(k),canvas.getTemplistIndex(m+1+j));
				j++;
			}
			k++;
		}
		while (i < n1 && Attributes.sorting) {
			
			list[k] = L[i];	
			canvas.addToTempElements(canvas.getTemplistIndex(k),canvas.getTemplistIndex(l+i));
			i++;
			k++;
		}
		while (j < n2 && Attributes.sorting) {
			
			list[k] = R[j];
			canvas.addToTempElements(canvas.getTemplistIndex(k),canvas.getTemplistIndex(m+1+j));
			j++;
			k++;
		}
		canvas.resetM();
		if(Attributes.step!=0)
		{
			Attributes.sorting=false;
			Attributes.pause();
		}

	}

	public void mergeSort(int l, int r) {
		if (l < r && Attributes.sorting == true) {
			int m = (l + r) / 2;
			canvas.addToTempD(m + 1);
			if(Attributes.step!=0)
			{
				Attributes.sorting=false;
				Attributes.pause();
			}
			mergeSort(l, m);
			mergeSort(m + 1, r);
			merge(l, m, r);
		}
		
	}
	
	public void insertionSort(int start, int end) {
		for (int i = start + 1; i <= end; i++) {
			canvas.setCurrent(i);
			int j = i;
			while (list[j] < list[j - 1] && Attributes.sorting) {
				swap(j, j - 1);
				canvas.swap(j, j-1);
				if (j != i) {
					canvas.setCheck(j);
				}
				if (j > start + 1)
					j--;
			}
			canvas.Update();
		}
		
	}
	public void bucketSort() {
		int counter = 0;
		for (int i = 0; i < list.length; i++) {
			canvas.setCheck(i);
			int j = list[i] / 10;
			canvas.addToBucket(j,i);
			if(Attributes.step!=0)
			{
				Attributes.sorting=false;
				Attributes.pause();
			}
		}
		canvas.setCheck(-1);
		Arrays.fill(list, -1);
		canvas.Update();
		for (int i = 0; i < 10; i++) {
			System.out.println(canvas.allBuckets.get(i).size());
			ArrayList<Element> clone = (ArrayList<Element>) canvas.allBuckets.get(i).clone();
			for (int j = 0; j < clone.size(); j++) {
				list[counter] = clone.get(j).getValue();
				canvas.removeBucket(i,clone.get(j));
				counter++;
				if(Attributes.step!=0)
				{
					Attributes.sorting=false;
					Attributes.pause();
				}
			}
		}
		insertionSort(0, list.length - 1);
		canvas.allBuckets.clear();
		Attributes.reset();
		canvas.Update();
	}
	public void swap(int i1, int i2) {
		int temp = list[i1];
		list[i1] = list[i2];
		list[i2] = temp;
	}
}
