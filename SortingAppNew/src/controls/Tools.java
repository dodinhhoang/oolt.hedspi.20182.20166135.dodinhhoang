package controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import models.Data;
import views.MainInterface;
public class Tools extends JPanel{
	private String[] types = { "Normal", "Step by step" };
	private String[] algorithms = { "Selection Sort", "Bubble Sort", "Merge Sort", "BucketSort" };
	private MainInterface canvas;
	public Tools(MainInterface canvas)
	{
		this.canvas = canvas;
		JPanel tools = this;
		int curAlg=0;
		JLabel delayL = new JLabel("Delay :");
		JLabel msL = new JLabel("");
		JLabel sizeL = new JLabel("Size :");
		JLabel lenL = new JLabel("");
		JLabel algorithmL = new JLabel("Algorithms");
		JLabel typeL = new JLabel("Mode");
		// DROP DOWN BOX
		JComboBox alg = new JComboBox(algorithms);
		JComboBox graph = new JComboBox(types);
		// BUTTONS
		JButton sort = new JButton("Sort");
		JButton shuffle = new JButton("Shuffle");
		JButton credit = new JButton("Credit");
		JButton back = new JButton("Back");
		JButton next = new JButton("Next");
		// SLIDERS
		JSlider size = new JSlider(JSlider.HORIZONTAL, 1, 6, 1);
		JSlider speed = new JSlider(JSlider.HORIZONTAL, 0, 10, 1);
		// SET UP TOOLBAR
		tools.setLayout(null);
		Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		tools.setBorder(BorderFactory.createTitledBorder(loweredetched, "Controls"));
		// SET UP ALGORITHM LABEL
		algorithmL.setHorizontalAlignment(JLabel.CENTER);
		algorithmL.setBounds(40, 20, 100, 25);
		tools.add(algorithmL);
		// SET UP DROP DOWN
		alg.setBounds(30, 45, 120, 25);// top=30,left=45
		tools.add(alg);
		// SET UP GRAPH TYPE LABEL
		typeL.setHorizontalAlignment(JLabel.CENTER);
		typeL.setBounds(40, 80, 100, 25);
		tools.add(typeL);
		// SET UP GRAPH TYPE DROP DOWN
		graph.setBounds(30, 105, 120, 25);
		tools.add(graph);

		// SET UP SORT BUTTON
		sort.setBounds(40, 150, 100, 25);
		tools.add(sort);

		// SET UP SHUFFLE BUTTON
		shuffle.setBounds(40, 190, 100, 25);
		tools.add(shuffle);

		// SET UP DELAY LABEL
		delayL.setHorizontalAlignment(JLabel.LEFT);
		delayL.setBounds(10, 230, 50, 25);
		tools.add(delayL);

		// SET UP MS LABEL
		msL.setHorizontalAlignment(JLabel.LEFT);
		msL.setBounds(135, 230, 25, 25);
		tools.add(msL);

		// SET UP SPEED SLIDER
		speed.setMajorTickSpacing(1);
		speed.setBounds(55, 230, 75, 25);
		speed.setPaintTicks(false);
		tools.add(speed);

		// SET UP SIZE LABEL
		sizeL.setHorizontalAlignment(JLabel.LEFT);
		sizeL.setBounds(10, 275, 50, 25);
		tools.add(sizeL);

		// SET UP LEN LABEL
		lenL.setHorizontalAlignment(JLabel.LEFT);
		lenL.setBounds(140, 275, 50, 25);
		tools.add(lenL);

		// SET UP SIZE SLIDER
		size.setMajorTickSpacing(10);
		size.setBounds(55, 275, 75, 25);
		size.setPaintTicks(false);
		tools.add(size);
		//SET UP BACK AND NEXT
		back.setBounds(10, 350, 75, 25);
		next.setBounds(95, 350, 75, 25);
		
		tools.add(back);
		tools.add(next);
		
		// SET UP CREDIT BUTTON
		credit.setBounds(40, 540, 100, 25);
		tools.add(credit);
		// ADD ACTION LISTENER
		size.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int val = size.getValue();
				if (size.getValue() == 5)
					val = 4;
				Data.len=val*10;
				//System.out.println(Attributes.len);
				lenL.setText(Data.len + "");
				if (Data.list.length !=Data.len)
					Data.shuffleList();
				Data.reset();
				//System.out.println(canvas);
				canvas.UpdateTools();
				
			}

		});
		sort.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!Data.sorting) {
					Data.sorting=true;
				}
				//System.out.println(Attributes.sorting);

			}
		});
		shuffle.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Data.shuffleList();
				//MainInterface.checklist.clear();
				Data.reset();
				canvas.UpdateTools();
			}
		});
		speed.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				Data.spd = (int) speed.getValue();
				msL.setText(Data.spd +" ");
			}
		});
		alg.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				Data.curAlg=alg.getSelectedIndex();	
				//System.out.println(Attributes.curAlg);
				canvas.UpdateTools();
			}
		});
		graph.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				Data.step=graph.getSelectedIndex();
				canvas.UpdateTools();
			}
		});
		next.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!Data.sorting) {
					Data.sorting=true;
				}
				//System.out.println(Attributes.sorting);

			}
		});
		back.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!Data.sorting) {
					Data.back=true;
					Data.list = (int[]) canvas.sort.pop();
					Data.sorting=true;
					canvas.Update();
					
				}
				//System.out.println(Attributes.sorting);

			}
		});
	}
}
