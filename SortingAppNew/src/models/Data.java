package models;

import java.util.ArrayList;
import java.util.Random;

public abstract class Data {
	//Class chua cac thuoc tinh dung chung cua cac class va bi thay doi khi chay chuong trinh
	public static int len=10;
	public static int list[];
	public static final int WIDTH = 1100;
	public static final int HEIGHT = 700;
	public static int width;
	public static int current = -1;
	public static int swap=-1;
	public static int swap1=-1;
	public static int check = -1;
	public static int spd=1;
	public static int curAlg=0;
	public static int step = 0;
	public static boolean back= false;
	public static boolean next = false;
	public static boolean pause = false;
	public static boolean sorting = false;
	public static boolean shuffled = false;
	public static void shuffleList()
	{
		Random r = new Random();
		list = new int[len];
		for (int a = 0; a < 500; a++) { // SHUFFLE RUNS 500 TIMES
			for (int i = 0; i < len; i++) { // ACCESS EACH ELEMENT OF THE LIST
				int rand = r.nextInt(100); // PICK A RANDOM NUM FROM 0-100
				list[i]= rand;
			}
		}
		sorting = false;
		shuffled = true;
	}
	public static void pause() {
		int i = 0;
		while (!sorting) { // LOOP RUNS UNTIL SORTING STARTS
			i++;
			if (i > 100)
				i = 0;
			try {
				Thread.sleep(1);
			} catch (Exception e) {
			}
		}
		// EXIT THE LOOP AND START SORTING
	}
	// RESET SOME VARIABLES
	public static void reset() {
		sorting = false;
		current = -1;
		check = -1;
		swap = -1;
		swap1= -1;
	}

	
}
