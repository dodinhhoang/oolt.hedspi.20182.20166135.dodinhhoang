package models;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

import alogorithms.BubbleSort;
import alogorithms.BucketSort;
import alogorithms.MergeSort;
import alogorithms.SelectionSort;

import java.awt.BorderLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import controls.Tools;
import views.MainInterface;

public class App{
	private MainInterface canvas;
	private Tools tools;
	JFrame jf;
	public App()
	{
		Data.shuffleList();
		jf = new JFrame();
		canvas = new MainInterface();
		tools = new Tools(canvas);
		jf.setSize(1400,700); //tao 1 khung cua so kich thuoc 500x500
        jf.setVisible(true); //hien thi khung cua so
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //thoat khoi chuong trinh khi nhan close
        jf.setLocation(0, 0);
        jf.setLayout(null);
        canvas.setBounds(200, 0, 1200,700);
        jf.add(canvas);
        tools.setBounds(0, 0, 200,700);
        jf.add(tools);
        sorting(canvas);
	}
	public void sorting(MainInterface canvas)
	{
		System.out.println(Data.sorting);
		if(Data.sorting)
		{
			int curAlg = Data.curAlg;
			//System.out.println(curAlg);
			if(curAlg==0) new SelectionSort(canvas).start();
			if(curAlg==1) new BubbleSort(canvas).start();
			if(curAlg==2) new MergeSort(canvas).start(0,Data.len-1);
			if(curAlg==3) new BucketSort(canvas).start();
		}
		Data.reset();
		pause();
	}
	public void pause() {
		int i = 0;
		while (!Data.sorting) { // LOOP RUNS UNTIL SORTING STARTS
			i++;
			if (i > 100)
				i = 0;
			try {
				Thread.sleep(1);
			} catch (Exception e) {
			}
		}
		sorting(canvas); // EXIT THE LOOP AND START SORTING
	}
	public static void main(String args[])
	{
		new App();
	}
}
