package hust.soict.hedspi.date;
import java.util.Scanner;

public class MyDate {
	private int day,month,year;
	
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public MyDate()
	{
		this.day=13;
		this.month = 4;
		this.year= 2019;
	}
	public MyDate(int day,int month,int year)
	{
		super();
		this.day=day;
		this.month = month;
		this.year= year;
	}
	public MyDate(String str)
	{
		super();
		String []data = new String [3];
		String []day = {
				"Not avaible",
				"1st",
				"2nd",
				"3rd",
				"4th",
				"5th",
				"6th",
				"7th",
				"8th",
				"9th",
				"10th",
				"11th",
				"12th",
				"13th",
				"14th",
				"15th",
				"16th",
				"17th",
				"18th",
				"19th",
				"20th",
				"21st",
				"22nd",
				"23rd",
				"24th",
				"25th",
				"26th",
				"27th",
				"28th",
				"29th",
				"30th",
				"31th",
};
		String[] month = {
				"Not avaible",
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
		};
		data = str.split(" ");
		for(int i=0 ;i<32;i++)
		{
			if(data[1].equals(day[i])==true)
			{
				this.day=i;
			}
		}
		for(int i=0;i<13;i++)
		{
			if(data[0].equals(month[i])==true)
			{
				this.month=i;
			}
		}
		this.year=Integer.parseInt(data[2]);
	}
	public void accept()
	{
		Scanner sc= new Scanner(System.in);
		System.out.println("Please input current date(eg:February 1st 2019):");
		String str = sc.nextLine();
		String []data = new String [3];
		String []day = {
				"Not avaible",
				"1st",
				"2nd",
				"3rd",
				"4th",
				"5th",
				"6th",
				"7th",
				"8th",
				"9th",
				"10th",
				"11th",
				"12th",
				"13th",
				"14th",
				"15th",
				"16th",
				"17th",
				"18th",
				"19th",
				"20th",
				"21st",
				"22nd",
				"23rd",
				"24th",
				"25th",
				"26th",
				"27th",
				"28th",
				"29th",
				"30th",
				"31th",
};
		String[] month = {
				"Not avaible",
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
		};
		data = str.split(" ");
		for(int i=0 ;i<32;i++)
		{
			if(data[1].equals(day[i])==true)
			{
				this.day=i;
			}
		}
		for(int i=0;i<13;i++)
		{
			if(data[0].equals(month[i])==true)
			{
				this.month=i;
			}
		}
		this.year=Integer.parseInt(data[2]);
		
		
	}
	public void print()
	{
		System.out.println(this.getDay()+"/"+this.getMonth()+"/"+this.getYear());
	}		

}
