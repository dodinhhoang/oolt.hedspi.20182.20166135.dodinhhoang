package hust.soict.hedspi.aims.Aims;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Track;
import hust.soict.hedspi.aims.order.Order.Order;

public class Aims {
	public static void showMenu() {
		System.out.println("Order Management Application");
		System.out.println("------------------------------");
		System.out.println("1.Create new order");
		System.out.println("2.Add item to the order");
		System.out.println("3.Delete item by id");
		System.out.println("4.Display the items list of order");
		System.out.println("0.Exit");
		System.out.println("---------------------------------");
		System.out.println("Please choose a number : 0-1-2-3-4");

	}
	public static void main(String args[])
	{
		Order anOrder= new Order();
		Scanner sc = new Scanner(System.in);
		int choice;
		int choice1;
		String title,category,authorName,director;
		List<String> author = new ArrayList<>();
		float cost,length;
		DigitalVideoDisc dvd;
		CompactDisc cd;
		Book book;
		while(true)
		{
			Aims.showMenu();
			choice = sc.nextInt();
			switch (choice) {
			case 1:
				anOrder.dateOrdered.accept();
				anOrder.dateOrdered.print();
				System.out.println("Order is created!!");
				break;
			case 2:
					System.out.println("What is kind of item?");
					System.out.println("1.Book");
					System.out.println("2.DigitalVideoDisc");
					System.out.println("3.CompactDisc");
					choice1 = sc.nextInt();
					switch(choice1)
					{
					case 1:
						sc.nextLine();
						System.out.println("Please input title:");
						title = sc.nextLine();
						
						System.out.println("Please input category:");
						
						category = sc.nextLine();
						System.out.println("Please input name of authors :");
						
						//System.out.println(book.getTitle()+book.getAuthors()+book.getCost()+book.getCategory());
						while(true)
						{
							
							authorName = sc.nextLine();						
							if(authorName.equals("stop"))
							{
								break;
							}else
							{
								author.add(authorName);
								
							}
							
						}
						System.out.println("Please input cost:");
						cost = sc.nextFloat();
						anOrder.addMedia(new Book(title,category,cost,author));
						System.out.println("OK!!");
						break;
					case 2:
						sc.nextLine();
						System.out.println("Please input title:");
						title = sc.nextLine();
						System.out.println("Please input category:");
						category = sc.nextLine();
						System.out.println("Please input director:");
						director = sc.nextLine();
						System.out.println("Please input length:");
						length = sc.nextFloat();
						System.out.println("Please input cost:");
						cost = sc.nextFloat();
						dvd = new DigitalVideoDisc(title,category,director,cost,length);
						anOrder.addMedia(dvd);
						System.out.println("Do you want to play video?\n1.Yes	2.No");
						if(sc.nextInt()==1)
						{
							dvd.play();
						}
						System.out.println("OK!!");
						break;
					case 3:
						sc.nextLine();
						System.out.println("Please input title");
						title = sc.nextLine();
						System.out.println("Please input Artist");
						director = sc.nextLine();
						System.out.println("Please input category");
						category = sc.nextLine();
						System.out.println("Please input cost");
						cost = sc.nextFloat();
						cd = new CompactDisc(title,category,director,cost);
						while(true)
						{
							System.out.println("Add tracks to CD?");
							System.out.println("1.Yes	2.No");
							if(sc.nextInt()==1)
							{
								sc.nextLine();
								System.out.println("Please input title:");
								title = sc.nextLine();
								System.out.println("Please input length:");
								length = sc.nextFloat();
								System.out.println("OK!!");
								cd.addTrack(new Track(title,length));
							}
							else break;
						}
						System.out.println("Do you want to play CD?\n 1.Yes	2.No");
						if(sc.nextInt()==1)
						{
							cd.play();
						}
						break;
					}
					break;
			case 3:
				System.out.println("Please input id of Item which you want to remove from order:");
				int index =sc.nextInt();
				if(index>anOrder.itemsOrdered.size())
				{
					System.out.println("index not available");
				}else
				{
					anOrder.removeMedia(index-1);
					System.out.println("Sucessfully!!");
				}
				break;
			case 4:
				anOrder.print();
				break;
			default:
				break;
			}
		}
	}

}
