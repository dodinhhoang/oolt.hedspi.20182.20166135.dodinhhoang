package hust.soict.hedspi.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable{
	private String artist;
	private float length;
	private ArrayList<Track> tracks= new ArrayList<>();
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public void addTrack(Track track)
	{
		if(!tracks.contains(track))
		{
			tracks.add(track);
			System.out.println("Sucessfully!!");
		}
		else
		{
			System.out.println("Track is existed !!");
		}
	}
	public void removeTrack(Track track)
	{
		if(tracks.contains(track))
		{
			tracks.remove(track);
			System.out.println("Sucessfully!!");
		}
		else
		{
			System.out.println("Track is not existed !!");
		}
	}
	public float getLength()
	{
		float totalLength=0;
		for(Track s:tracks)
		{
			totalLength+=s.getLength();
		}
		return totalLength;
	}
	
	public CompactDisc(String title,String category,String artist, float cost) {
		super();
		this.setCost(cost);
		this.setArtist(artist);
		this.setCategory(category);
		this.setTitle(title);
		this.artist = artist;
		
	}
	@Override
	public void play() {
		// TODO Auto-generated method stub
		for(Track t:tracks)
		{
			t.play();
		}
		
	}
	

}
