package hust.soict.hedspi.aims.media;

public class DigitalVideoDisc extends Disc implements Playable{
	
	public boolean search(String str)
	{
		
		String []temp;
		int check=0,count =0;
		temp = str.split(" ");
		for(String s:temp)
		{
			if(this.getTitle().contains(s)==true) {
				check++;
			}
			count ++;
		}
		if(check == count) return true;
		return false;
	}
	
	public DigitalVideoDisc(String title,String category,String director,float length,float cost)
	{
		super(title,category,director,length,cost);
	}
	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing DVD: "+this.getTitle());
		System.out.println("DVD length: "+this.getLength());	
	}
	
}
