package hust.soict.hedspi.aims.media;

public class Track implements Playable{
	private String title;
	private float length;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public float getLength() {
		return length;
	}
	public void setLength(float length) {
		this.length = length;
	}
	public Track()
	{
		
	}
	public Track(String title,float length)
	{
		this.setTitle(title);
		this.setLength(length);
	}
	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing Track:"+this.getTitle());
		System.out.println("Track length:"+this.getLength());
		
	}
	
	

}
